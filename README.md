[![Visual Studio](https://img.shields.io/badge/Visual%20Studio-2022-5C2D91.svg?style=flat&logo=visualstudio)](https://visualstudio.microsoft.com/)

# CSharpTester

Main goal of this is to generate `.dll` file, so the Mono Runtime
can load the assembly!

## Usage

Just open up in the Visual Studio and compile it to generate `.dll` file!
